window.onload = function(){
	var controls = $(".tabs-controls__item");
	var content = $(".tabs-content__item");
	controls.on("click", function(){
		controls.removeClass("active")
		$(this).addClass("active");
		var i = ($(this).index());
		content.removeClass('show show animated flipInX');
		content.eq(i).addClass('show animated flipInX');
	})
}
